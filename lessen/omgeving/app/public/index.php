<?php

$request_uri = explode('?', $_SERVER['REQUEST_URI'], 2);

// Simpele Routing:
switch ($request_uri[0]) {
    case '/':
        header('location: /cursus');
        break;
    case '/blog':
        require 'voorbeeld/blog/index.php';
        break;
    default:
        header('HTTP/1.0 404 Not Found');
        require 'templates/404.php';
        break;
}

exit;

//header('location: /cursus');
//echo phpinfo();
//exit;