<?php if(isset($_GET['success'])): ?>

<div class="success-msg">Login successful</div>

<?php else: ?>

<?php if(isset($_GET['fail'])): ?>

<div class="error-msg">Login failed</div>

<?php endif; ?>

<form id="form" action="/blog/login-action.php" method="post">
    <label for="username">Username</label>
    <input type="text" id="username" name="username" placeholder="Username" required>

    <label for="password">Password</label>
    <input type="password" id="password" name="password" placeholder="Password" required>

    <input type="submit" value="Submit">
</form>

<?php endif; ?>

<style>
	.error-msg {
		color: #D8000C;
  		background-color: #FFBABA;
  		margin: 10px 0;
  		padding: 10px;
  		border-radius: 3px 3px 3px 3px;
	}

	.success-msg {
		color: #270;
		background-color: #DFF2BF;
		margin: 10px 0;
		padding: 10px;
	    border-radius: 3px 3px 3px 3px;
	}
</style>

