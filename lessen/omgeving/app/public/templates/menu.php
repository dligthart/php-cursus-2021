<div class="container">
	<header class="d-flex flex-wrap justify-content-center py-3 mb-4">
	    <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
	      <img id="logo" class="bi me-2" src="/img/logo.svg" alt="AvansPlus" width="120" />
	      <span class="fs-4">PHP Cursus</span>
	    </a>
	     <ul class="nav nav-pills">
	      <li class="nav-item"><a href="/cursus" class="nav-link active">Home</a></li>
	    </ul>
	</header>
</div>

<div class="container">