        </div>
    </main>

    <footer class="footer mt-auto py-3 bg-light">
      <div class="container">
        <span class="text-muted">&copy; <?php echo date('Y'); ?> - PHP Cursus door Dave Ligthart</span>
      </div>
    </footer>

    <!-- EnlighterJS -->
    <script type="text/javascript" src="/js/enlighterjs/dist/enlighterjs.min.js"></script>
    <script type="text/javascript">
        // - highlight all pre + code tags (CSS3 selectors)
        // - use javascript as default language
        // - use theme "enlighter" as default theme
        // - replace tabs with 2 spaces
        EnlighterJS.init('pre', 'code', {
            language : 'php',
            theme: 'dracula',
            indent : 2
        });
    </script>

</body>
</html>