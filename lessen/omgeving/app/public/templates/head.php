<!DOCTYPE html>
<html>
<head>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/js/enlighterjs/dist/enlighterjs.min.css" rel="stylesheet">
	<style>
		body {
			background-color: #f2f2f2;
		}
		
		.card:hover {
			transform: scale(1.05);
		  	box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
		  	cursor:pointer;
		}
	</style>

</head>
<body>
    <main>