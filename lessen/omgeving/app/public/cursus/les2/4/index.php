<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les2">Terug naar overzicht</a>
  
<h1 class="py-5 pb-2 border-bottom">Les 2.4</h1>

<h2 class="py-5 pb-2">Foreign keys</h2>

<p>
Allereerst zal ik even in een notendop uitleggen wat Foreign Keys eigenlijk zijn:</p>
<p>
Foreign Keys (oftewel verwijzende sleutels), ook wel afgekort tot FK's, duiden relaties aan tussen twee tabellen in een database. Een waarde in een tabel zal verbonden worden met een andere tabel door twee waardes altijd overeen te laten komen.
</p>

<p>
Nu we weten waar FK's voor gebruikt worden willen we natuurlijk weten hoe we deze relatie tussen twee tabellen definiëren.
Dit gaat verrassend simpel; ik ga in ons geval een relatie aanbrengen tussen twee tabellen, namelijk: "posts" en "comments".
</p>

<p>
Hieronder staan de twee tabellen ZONDER Foreign Keys, zodat we een beeld krijgen waarmee we gaan werken.
</p>

<pre data-enlighter-language="mariadb">
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;

</pre>

<p>
	De oplettende lezer zal het veld "post_id" in de tabel comments wel opgevallen zijn. Hiermee gaan we een comment aan een post verbinden, de relatie tussen posts en comments.
</p>

<pre data-enlighter-language="mariadb">
ALTER TABLE `comments` ADD INDEX ( `post_id` )
ALTER TABLE `comments` ADD FOREIGN KEY ( `post_id` ) REFERENCES `posts` ( `id` );
</pre>

<p>Door een foreign key aan te brengen bewaken we de integriteit van de data zodat je niet zomaar data kan verwijderen. In dit geval wanneer je een post wilt verwijderen zul je eerst alle comments moeten verwijderen die aan een specifieke post verbonden zijn</p>

<p>Het maakt het ook mogelijk om via een join query alle data te halen:</p>

<p>Haal alle comments op met bijbehorende post data:</p>

<pre data-enlighter-language="mariadb">
SELECT * FROM posts as A JOIN comments as B ON A.id = B.post_id
</pre>

<?php include '../../../templates/footer.php'; ?>