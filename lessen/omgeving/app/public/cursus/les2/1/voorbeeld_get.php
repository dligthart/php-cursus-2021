
<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>


<div class="container">

<a href="index.php">Terug naar theorie</a>

<?php

// Voorbeeld gebruik $_GET variabele.

echo '<p>Zie de url parameters die zijn toevoegd aan de url hierboven in de locatiebalk. <br/>Probeer de waarden van de variabelen te veranderen in de URL en zie wat er gebeurd.</p>';

foreach($_GET as $key=>$value) {
	echo $key . '=' . $value;
	echo '<br/>';
}

?>

</div>

<?php include '../../../templates/footer.php'; ?>