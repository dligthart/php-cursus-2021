<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les2">Terug naar overzicht</a>
  
<h1 class="py-5 pb-2 border-bottom">Les 2.2 - SQL</h1>

<h2 class="py-5 pb-2">Select Query</h2>

<p>Het SELECT statement wordt gebruikt om gegevens te selecteren van een tabel.</p>

<pre data-enlighter-language="mariadb">
SELECT * FROM blog.comments
</pre>

<pre data-enlighter-language="mariadb">
SELECT * FROM blog.comments ORDER BY created_date DESC
</pre>

<h2 class="py-5 pb-2">Insert Query</h2>

<p>Het INSERT statement wordt gebruikt om informatie toe te voegen aan je<br/> database. De standaard syntax voor dit statement gaat als volgt:

<pre data-enlighter-language="mariadb">
INSERT INTO tabelnaam (kolom1, kolom2) VALUES ('waarde1', 'waarde2')
</pre>

</p>

<p>Voorbeeld:</p>
<pre data-enlighter-language="mariadb">
INSERT INTO blog.comments ( name, email, website, created_date, message)  VALUES ( ‘Piet, ’piet@google.com', ‘www.google.com’, ’2021-04-02 00:00:00’, ’Hallo Wereld, dit is Piet.’)
</pre>

<h2 class="py-5 pb-2">Update Query</h2>

<p>Het is heel handig wanneer je gegevens kan updaten, bijvoorbeeld wanneer iets niet goed is.
    <br/> In dit geval maak je gebruik van het UPDATE statement.
    <br/>Hiermee zorg je dat waardes uit de tabel gewijzigd kunnen worden.
</p>

<pre data-enlighter-language="mariadb">
UPDATE tabelnaam SET kolom1='waarde1', kolom2='waarde2' WHERE kolom3='waarde3'
</pre>

<pre data-enlighter-language="mariadb">
UPDATE blog.comments  SET email=‘joop@google.com’  WHERE id=1
</pre>


<h2 class="py-5 pb-2">Delete Query</h2>

<p>Wanneer je gegevens hebt staan in de database die je niet meer wilt hebben,
    dan kun je deze verwijderen met behulp van het DELETE statement.
    Houd er wel rekening mee dat verwijderde gegevens ook echt verwijderd zijn!
</p>
<pre data-enlighter-language="mariadb">
DELETE FROM blog.comments  WHERE email = ‘joop@google.com’
</pre>

<?php include '../../../templates/footer.php'; ?>