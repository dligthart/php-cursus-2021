<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>
 
<div class="container">
 
	<a href="/cursus/les2">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 2 - Opdracht</h1>

	<h2 class="py-5 pb-2">Schrijf een programma met de volgende functionaliteiten:</h2>

	<ol>
		<li>Een admin kan via een formulier inloggen met een gebruikersnaam en wachtwoord</li>
		<li>De gebruikers worden in een nieuwe database tabel opgeslagen genaamd "users"
		</li>
		<li>De "posts" tabel krijgt als foreign key de user_id zodat je weet wie de blog post heeft aangemaakt.</li>
		<li>De admin kan een overzicht inzien van alle blog posts</li>
		<li>De admin kan een blog post verwijderen</li>
		<li>De admin kan een blog post wijzigen: de status van "draft" naar "published", de titel, de content, de modified date.</li>
		<li>De admin kan een blog post aanmaken.</li>
		<li>De admin kan een overzicht zien van gebruikers</li>
		<li>De admin kan gebruikers toevoegen, wijzigen en verwijderen</li>
		<li>De bezoeker kan op de homepage alle blog posts bekijken met als status published</li>
		<li>De bezoeker kan een comment achter later per blog post</li>
		<li>De admin kan een overzicht zien van alle comments en kan deze goedkeuren of afkeuren</li>
		<li>De admin kan uitloggen.</li>
		<li>Gebruik functies uit de vorige opdracht om comments automatisch te filteren op groftaalgebruik voordat het in de database terecht komt.</li>
	</ol>

</div>

<?php include '../../../templates/footer.php'; ?>