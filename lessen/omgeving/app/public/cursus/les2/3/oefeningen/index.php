<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les2">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 2.3 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

	<p>
	Schrijf een functie waarmee de rijen en kolommen uit de comments tabel van de database in een html tabel worden getoond.
	</p>

	<h2 class="py-5 pb-2">Oefening 2</h2>

	<p>
	Schrijf een functie waarmee je nieuwe records kan toevoegen aan de comments tabel.
	</p>

	<h2 class="py-5 pb-2">Oefening 3</h2>

	<p>
	Neem het formulier uit 2.1 en maak het zo dat er bij iedere form submit een nieuw record aan de database wordt toegevoegd met de data die is ingevoerd in de velden.
	</p>

</div>

<?php include '../../../../templates/footer.php'; ?>