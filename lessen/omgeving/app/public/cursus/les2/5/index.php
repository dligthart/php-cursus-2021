<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les2">Terug naar overzicht</a>
  
<h1 class="py-5 pb-2 border-bottom">Les 2.5 - Sessions</h1>

<h2 class="py-5 pb-2">Sessie starten</h2>

Wanneer je een sessie start kun je de waarden uit de globale variabele $_SESSION op iedere pagina raadplegen en zo kun je checken of een gebruiker bijvoorbeeld is ingelogd. En je kan evt nog andere data meegeven.

<figure>
  <figcaption>Voorbeeld code</figcaption>
  <pre>

session_start();

$_SESSION['username'] = 'Piet';
$_SESSION['loggedin'] = true;
$_SESSION['time']     = time();


  </pre>
</figure>

<h2 class="py-5 pb-2">Sessie stoppen</h2>

Wanneer je een sessie wilt stoppen dan kun je onderstaande code aanroepen om de data uit de sessie te verwijderen. Dit doe je bijvoorbeeld wanneer de gebruiker uitlogd.

<figure>
  <figcaption>Voorbeeld code</figcaption>
  <pre>

session_start();

$_SESSION = array();

session_destroy();

  </pre>
</figure>

<?php include '../../../templates/footer.php'; ?>