<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les2">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 2.5 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

<ul>
	<li>
	Maak een pagina "login.php" aand met een formulier waarmee je een gebruikersnaam en wachtwoord kan invoeren. 
	</li>
	<li>
	De toegestane gebruikersnaam en wachtwoord mag je in een constante zetten.
	</li>
	<li>
	Check of het wachtwoord en gebruikersnaam constanten overeenkomen met de ingevoerde velden uit het formulier.
	</li>
	<li>
	Als de gegevens kloppen sla in de sessie op dat de gebruiker is ingelogd.
	</li>
	<li>Wanneer de gegevens niet overeenkomen toon je een melding aan de gebruiker dat de gegevens niet kloppen.</li>
	<li>
	Maak vervolgens een nieuwe php pagina genaamd "logout.php" aan en start een sessie en check of de gebruiker is ingelogd.  
	</li>
	<li>
	Toon vervolgens een uitlog knop en maak deze functioneel.
	</li>
</ul>

</div>

<?php include '../../../../templates/footer.php'; ?>