<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

	<a href="/cursus/les3">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 3 - Opdracht</h1>

	<h2 class="py-5 pb-2">Het blog systeem uitbreiden met de volgende functionaliteiten:</h2>

	<ol>
		<li>Herschrijf jouw blogsysteem uit de weekopdracht van les 2 door gebruik te maken van een object-georienteerde aanpak.</li>
                <li>Maak een database klasse en interface (uit oefening 4)</li>
                <li>Maak een model interface (uit oefening 4)</li>
                <li>Maak User, Post en Comment Models (uit oefening 4 en breidt uit)</li>
                <li>Maak een Config klasse met daarin constanten met oa connectie informatie van de database. En pas de database klasse hierop aan. Denk aan een setter voor de Config.</li>
                <li>Zorg ervoor dat je de klassen, interfaces etc in aparte bestanden zet en include. Zet deze weg in een logische mappen structuur.</li>
                <li>Maak een klasse die de gegevens uit de verschillende models kan tonen in een tabel. (Maak gebruik van dependency injection (DI) ). Bijvoorbeeld een Table klasse met een publieke "render" methode en een set methode voor het Model. </li>
                <li>Maak een klasse voor Sessions waarmee je sessie kunt bijhouden, starten, stoppen, controleren, updaten etc.</li>
                <li>Maak een Authentication klasse die een Credentials klasse verwacht in de constructor. De Credentials kunnen de gebruikersnaam en wachtwoord bevatten. De Authentication klasse kan de sessie controleren op login status en heeft een login en logout methode.</li>
                <li>Maak een klasse ProfanityFilter obv de functies van opdracht les 1 en gebruik deze in de Post en Comment klasse dmv DI. Zorg ervoor dat deze overeft van de superklasse Filter. Zodat je later ander soortige filters kunt maken. Je mag hier ook gebruik maken van een interface. </li>
                <li>Maak een homepage met een overzicht van alle blog posts met een samenvatting.</li>
                <li>Een blog post linkt naar een pagina waar je de volledige blog post kan lezen.</li>
                <li>Toon alle comments op deze pagina onder de blog post.</li>
                <li>Toon ook een formulier waar je een nieuwe comment kan toevoegen.</li>
	</ol>

<?php include '../../../templates/footer.php'; ?>