<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les3">Terug naar overzicht</a>

<h1 class="py-5 pb-2 border-bottom">Les 3.2 - Inheritance (Overerving)</h1>

<p>
    Het woord inheritance (overerving) zegt het eigenlijk al.
    Een class die overerft van een parent class neemt bepaalde variabelen (Eigenschappen)
    en methods (Gedrag) over van de parent class.
</p>

<p>
    Object georiënteerd programmeren is erg gebaseerd op de werkelijkheid.
    Bij inheritance zie je dit goed terug.
</p>

<p>
    In de ondertaande code zie je dat RaceAuto overerft van Auto.
    De Auto class bevat eigenschappen en gedrag die elke auto zou moeten hebben.
    In dit geval gaan we er van uit dat elke Auto een type, kleur, wielen en snelheid heeft.
</p>

<strong>Voorbeeld:</strong>

<pre data-enlighter-language="php">
/**/

// Superklasse.
class Auto {

    // De subklasse kan bij een protected property.
    protected $snelheid = 0;

    // De subklasse kan niet bij een private property.
    private $wielen = 4;
    private $type = 'standaard';
    private $kleur = 'geen';

    public function __construct($type, $kleur, $snelheid) {
        $this->type = $type;
        $this->kleur = $kleur;
        $this->snelheid = 0;
    }

    public function tonen() {

        $autoKleur = $this->kleur;

        if('rood' == $this->kleur) {
            $autoKleur = 'rode';
        } elseif ('kanariegeel' == $this->kleur) {
            $autoKleur = 'kanariegele';
        } else {
            $autoKleur .= 'e';
        }

        echo "<p>Dit is een {$autoKleur} {$this->type} met $this->wielen wielen.</p>";
    }

    public function toonSnelheid() {
        if($this->snelheid > 0) {
            echo "<p>De auto gaat nu {$this->snelheid} km per uur.</p>";
        } else {
            echo "<p>De auto is klaar voor de start.</p>";
        }
    }
}

// Subklasse.
class RaceAuto extends Auto {

    /**
     * Via de constructor kan ik bij de private properties
     * van Auto en zo de waarden veranderen.
     */
    public function __construct($kleur) {

        parent::__construct('race auto', $kleur, 0); // met de double colon operator '::' kun je de methodes aanroepen van de superklasse.

        // echo parent::$kleur; // Dit geeft een error wanneer je het uncomment. Vanwege de private modifier.
    }

    /**
     * De snelheid property kan door de subklasse
     * van Auto worden gewijzigd omdat deze protected is.
     *
     * Deze methode is specifiek voor klasse RaceAuto.
     */
    public function boostSpeed() {
        $this->snelheid = 200;
    }
}

echo "<strong>superklasse Auto</strong>";

$station = new Auto('station wagon', 'zwart', 0);
$station->tonen(); // Dit is een zwarte station wagon met 4 wielen.

echo "<strong>subklasse RaceAuto</strong>";

$raceAuto = new RaceAuto('rood');
$raceAuto->tonen();
$raceAuto->toonSnelheid();

$raceAuto->boostSpeed(); // Hiermee wijzig ik de snelheid property van de superklasse.
$raceAuto->toonSnelheid();

$raceAuto->snelheid = 1000; // Dit geeft dus een error vanwege de protected modifier. Hiermee voorkomen we fouten door verkeerd gebruik van de data.

/**/
</pre>

<p>
    Je kunt dus een nieuwe klasse aanmaken op basis van de klasse RaceAuto.
    Deze erft dan alle eigenschappen en gedrag van zowel Auto als RaceAuto.
</p>

<p>
    Zoals je in onderstaand voorbeeld kunt zien is dat je door overerving steeds
    concretere klassen kan definieren:
</p>

<strong>Auto <- RaceAuto <- Lamborghini.</strong>
<br/>
<strong>Voorbeeld</strong>

<pre>
/**/

class Lamborghini extends RaceAuto {

    public function __construct() {
        parent::__construct('kanariegeel');
        $this->snelheid = 300; // gaat standaard full throttle :)
    }

    /**
     * Hiermee kun je de methode "tonen" overschrijven en je eigen impementatie maken.
     */
    public function tonen() {
        parent::tonen(); // Hiermee roep je de methode van de superklasse Auto aan.
        echo '<p>Dit is een race auto van het merk Lamborghini.</p>';
    }
}

$lambo = new Lamborghini();
$lambo->tonen();
$lambo->toonSnelheid();

/**/
</pre>

<p>
Een volgende logische stap in de klasse hierarchie kan dan bijvoorbeeld zijn:
</p>

    <strong>Auto <- RaceAuto <- Lamborghini <- LamborghiniUrus.</strong>
<p>&nbsp;</p>
<?php include '../../../templates/footer.php'; ?>