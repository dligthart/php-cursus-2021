<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les3">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 3.2 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

    <ul>
        <li>Maak 3 verschillende subklassen van Dier</li>
        <li>Maak een methode welke acties het dier aan het ondernemen is bijvoorbeeld, eten, lopen, rennen, vliegen, springen etc</li>
        <li>Schrijf een methode waarmee je de eigenschappen en acties van het Dier kunt tonen.</li>
    </ul>

</div>

<?php include '../../../../templates/footer.php'; ?>