<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les3">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 3.1 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

    <ul>
        <li>Maak een klasse genaamd Dier</li>
        <li>Geef deze 5 verschillende properties zoals het diersoort etc.</li>
        <li>Maak de properties private.</li>
        <li>Zorg ervoor dat je via de constructor de properties kunt wijzigen.</li>
        <li>Schrijf een methode waarmee je de eigenschappen van het Dier kunt tonen.</li>
        <li>Instantieer 5 verschillende Dier objecten met unieke eigenschappen.</li>
    </ul>

</div>

<?php include '../../../../templates/footer.php'; ?>