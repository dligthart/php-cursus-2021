<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

	<a href="/cursus/les3">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 3.1 - Object-oriented Programming (OOP)</h1>

    <h2 class="py-5 pb-2">Classes</h2>

    <p>
        PHP is een object-georienteerde programmertaal waar er gebruik wordt gemaakt van objecten.
        Zoals in de echte wereld. Een blauwe stoel. Een ronde tafel. etc
    </p>

    <p>
        Software objecten hebben eigenschappen en gedrag.
    </p>

    <p>
        Zonder classes kunnen er geen objecten gemaakt worden. Een class is een blauwdruk voor objecten.
        In een class staan properties (variabelen) en methods (functies).
        Alle objecten die gecreëerd worden op basis van een class, kunnen gebruik maken van de properties en methods die in de class zijn opgesteld.

    </p>

    <p>
        Een eenvoudige class bevat het volgende:
    </p>

    <ul>
        <li>De naam van de klasse</li>
        <li>Een body. De body is omringd door {}.</li>
        <li>Properties en methodes met een modifier (public, protected of private)</li>
        <li>Een constructor.</li>
    </ul>

<strong>Voorbeeld</strong>
<pre data-enlighter-language="php">
/**/

class Tafel {

    public $aantal_poten = 4;
    public $vorm = 'rond';
    public $kleur = 'blauw';

    public function tonen() {
        echo "Dit is een {$this->kleur}e {$this->vorm}e tafel met $this->aantal_poten poten.";
    }
}

$tafel = new Tafel;
$tafel->tonen(); // Dit is een blauwe ronde tafel met 4 poten.

/**/
</pre>

    <h2>Constructor</h2>

    <p>Een constructor is een speciale functie binnen de class om het object te initialiseren met specifieke eigenschappen.</p>

    <p>Wanneer een object van een class wordt aangemaakt (geinstantieerd) met het "new" keyword wordt de constructor methode automatisch aangeroepen.</p>

<pre data-enlighter-language="php">
/**/

class Tafel {

    public $aantal_poten = 4;
    public $vorm = 'rond';
    public $kleur = 'blauw';

    public function __construct($vorm, $kleur) {
        $this->vorm = $vorm;
        $this->kleur = $kleur;
    }

    public function tonen() {
        echo "Dit is een {$this->kleur}e {$this->vorm}e tafel met $this->aantal_poten poten.";
    }
}

$tafel = new Tafel('vierkant', 'groen'); // Hiermee roep je de constructor aan.
$tafel->tonen(); // Hiermee roep je de public functie van een klasse aan.

/**/
</pre>
<p>
Met het de arrow <em>'->'</em> kun je instance members (variabelen en methoden) aanroepen
van de geinstantieerde klasse (object).
</p>
<pre data-enlighter-language="php">
/**/

$tafel->tonen();

/**/
</pre>

    <h2>Visibility</h2>

    <p>Access modifiers zijn keywords waarmee de toegankelijkheid en zichtbaarheid van onder andere classes, properties en methods bepaald worden.</p>

    <p>Access modifiers maken "encapsulation" mogelijk waarmee voorkomt
        dat data makkelijk te manipuleren is buiten de class.
        Soms wil je niet dat iedereen data buiten een class makkelijk kan manipuleren
        en dan is encapsulation heel handig.
    </p>
    <p>
        Je kan er bijvoorbeeld ook voor kiezen om bepaalde variabelen alleen "leesbaar"
        te maken en niet de mogelijkheid geven om het te manipuleren.
        De gebruiker hoeft door encapsulation geen weet te hebben van de totale werking van de class.
    </p>
    <ul>
        <li><strong>public</strong>:  Is overal toegankelijk.</li>
        <li><strong>private</strong>:  Is alleen in de eigen class toegankelijk.</li>
        <li><strong>protected</strong>: Is alleen de eigen class en child classes toegankelijk.</li>
    </ul>

    <strong>Voorbeeld</strong>
<pre data-enlighter-language="php">
/**/

class Voorbeeld {

    private $privateVar = 'private var';
    public $publicVar = 'public var';

    private function privateMethod() {
        echo "Toon Private";
    }

    public function publicMethod() {
        echo "Toon Public";
    }
}

$voorbeeld = new Voorbeeld;

echo $voorbeeld->publicVar;
echo $voorbeeld->privateVar; // geeft een error omdat deze niet toegankelijk voor de buitenwereld is.

$voorbeeld->publicMethod();
$voorbeeld->privateMethod(); // geeft een error omdat deze niet toegankelijk voor de buitenwereld is.

/**/
</pre>

<?php include '../../../templates/footer.php'; ?>