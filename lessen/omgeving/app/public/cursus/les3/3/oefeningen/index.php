<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les3">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 3.3 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

    <ul>
        <li>Schrijf getters en setters voor de properties van de superklasse Dier en maak hier gebruik van vanuit de subklassen.</li>
        <li>Maak constanten van de acties die het dier kan doen en breng die aan in de subklassen. De acties moeten wel passen bij het type dier. Dus een vogel kan vliegen, lopen, een vis kan zwemmen, duiken etc.</li>
        <li>Schrijf een methode om willekeurig acties van het dier te tonen.</li>
    </ul>

</div>

<?php include '../../../../templates/footer.php'; ?>