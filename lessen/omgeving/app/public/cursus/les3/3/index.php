<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les3">Terug naar overzicht</a>

<h1 class="py-5 pb-2 border-bottom">Les 3.3 - Getters en Setters</h1>

<p>
    Properties worden bereikbaar en te manipuleren door public getter en setter methodes.
    Dit zijn methodes die je herkent aan de “get” of  “set” vooraan in de naam van de methode.

    Met de get en set methoden bepaal jij de toegangsrechten van de data
    om van buitenaf geschreven of gelezen te worden.
</p>

<p>
Met de public getters kan je zowel in, als buiten de class de waarde van de variabele ophalen.
Met de public setters kan je een waarde geven aan de property van de klasse.
</p>

<p>
    <em>$this</em> verwijst naar de instantie van de klasse binnen de klasse.
</p>

<strong>Voorbeeld</strong>

<pre data-enlighter-language="php">
/**/

class Auto {

    private $type = 'hatchback';
    private $kleur = 'geen';

    // In dit geval mogen we alleen de kleur van de auto aanpassen.
    public function setKleur($kleur) {
        $this->kleur = $kleur;
    }

    public function getKleur() {
        return $this->kleur;
    }

    public function getType() {
        return $this->type;
    }

    public function tonen() {
        echo 'Deze ' . $this->getType() . ' heeft de kleur ' . $this->getKleur();
    }
}

$auto = new Auto;
$auto->setKleur('groen');
$auto->tonen();

 /**/
</pre>

<h2>Class Constants</h2>

<p>
    Constanten kunnen niet veranderd worden als ze eenmaal zijn gedeclareerd.
    Dit is handig als je statische informatie wilt vastleggen zoals code, berichten etc.
</p>

<strong>Voorbeeld</strong>

<pre>
/**/

class Status {
    const ONLINE = 'online';
    const OFFLINE = 'offline';
    const MESSAGE = 'Deze website is nu ';

    private $status;

    public function __construct() {
        $this->status = self::OFFLINE;
    }

    public function toggle() {
        if($this->status == self::ONLINE) {
            $this->status = self::OFFLINE;
        } else {
            $this->status = self::ONLINE;
        }
    }

    public function getMessage() {
        return self::MESSAGE . $this->status;
    }
}

$status = new Status();
$status->toggle();
echo $status->getMessage(); // Deze website is nu online

/**/
</pre>

<?php include '../../../templates/footer.php'; ?>