<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les3">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 3.4 - Oefeningen</h1>


	<h2 class="py-5 pb-2">Oefening 1</h2>

	<p>
	Schrijf een MysqlDatabase klasse met methoden:
    </p>
    <ul>
        <li>waarmee je een connectie kunt maken met de database</li>
        <li>waarmee je een connectie kunt verbreken met de database</li>
        <li>records kan ophalen uit de database</li>
    </ul>


	<h2 class="py-5 pb-2">Oefening 2</h2>

	<p>
	Maak een User, Post en Comment klasse waarmee je rijen uit de database kunt halen. Maak gebruik van een interface en DI (dependency injection) van de database (zie theorie).
	</p>

	<h2 class="py-5 pb-2">Oefening 3</h2>

	<p>
    Voeg methodes toe aan de interface van de database:
    <ul>
            <li>voor het verwijderen van records op basis van id</li>
            <li>voor het updaten van bestaande records op basis van id</li>
            <li>voor het aanmaken van nieuwe records</li>
    </ul>

	</p>

    <h2 class="py-5 pb-2">Oefening 4</h2>

    <p>
        Implementeer de methodes van de interface in de MysqlDatabase klasse.
        En zorg ervoor dat de User, Post en Comment klasse hiervan gebruik maken.
    </p>

    <h2 class="py-5 pb-2">Oefening 5</h2>

    <p>
        Maak een interface "Model" en implementeer deze voor de User, Post en Comment.

        Denk aan methodes die User, Post en Comment delen als:
        <ul>
            <li>fetchOne($id) { return $record; }</li>
            <li>fetchAll() { return array(); }</li>
            <li>deleteOne($id) { return $success; } </li>
            <li>update($id, $properties = array()){ return $success; }</li>
            <li>add(){ return $id; }</li>
        </ul>

    </p>

</div>

<?php include '../../../../templates/footer.php'; ?>