<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

    <a href="/cursus/les3">Terug naar overzicht</a>

    <h1 class="py-5 pb-2 border-bottom">Les 3.4 - Dependency Injection</h1>

    <p>Dit is een software design pattern waarmee je hardgecodeerde afhankelijkheden kunt voorkomen.</p>

    <ul>
        <li>Modulair: hiermee maak je opzichzelfstaande klassen aan.</li>
        <li>Testbaar: Je kan de code makkelijker testen</li>
        <li>Onderhoudbaar: Het is makkelijker om de code te fixen of uit te breiden.</li>
    </ul>

    <h2>Probleem schets</h2>

    <p>In onderstaand voorbeeld is er een hardekoppeling tussen User en Database. </p>

    <p>
        Stel dat we op een later moment de database gegevens willen wijzigen dan moeten we dit dus in de User klasse doen.
        Stel dat meerdere klassen op deze manier implementeren doen dan moeten we de gegevens in iedere klasse wijzigen.
    </p>

    <p>Dit is niet wenselijk.</p>

    <strong>Voorbeeld 1 - Zonder dependency injection</strong>

<pre data-enlighter-language="php">
/**/

class Database {
    private $connection = null;

    public function __construct($host, $username, $password, $databaseName)
    {
    }

    public function getAll($tableName) {
        return array('Piet', 'Kees', 'Annabel');
    }
}

class User
{
    private $database = null;

    // De User klasse moet dus beschikken over de database connectiegegevens.
    public function __construct() {
        $this->database = new Database('host', 'user', 'pass', 'dbname');
    }

    public function getUsers() {
        return $this->database->getAll('users');
    }
}

$user = new User();
print_r($user->getUsers());

/**/
</pre>

    <strong>Voorbeeld 1 - Mét dependency injection</strong>

    <p>
        De user klasse hoeft alleen maar te weten dat er een database is.
        De connectie informatie, het type database etc zijn verborgen voor de klasse.
    </p>

<pre data-enlighter-language="php">
/**/

class User
{
    private $database = null;

    public function __construct(Database $database) {
        $this->database = $database;
    }

    // Hier hoeft de User klasse alleen maar te weten hoe gegevens vanuit de database moeten worden opgehaald.
    public function fetch() {
        return $this->database->getAll('users');
    }
}

class Post
{
    private $database = null;

    public function __construct(Database $database) {
        $this->database = $database;
    }

    public function fetch() {
        return $this->database->getAll('posts');
    }
}

$database = new Database('host', 'user', 'pass', 'dbname');

$user = new User($database);
$post = new Post($database);

$user->fetch();
$post->fetch();

/**/
</pre>

    <h2>Interfaces</h2>

    <p>Met interfaces kun je code maken waarmee je kan specificeren welke methodes een klasse moet implementeren.</p>

    <p>Een klasse moet <strong>alle</strong> methods implementeren van de interface.</p>

    <p>Dat doe je dmv het "implements" keyword: <strong>class A implements iA {}</strong> </p>

    <strong>Voorbeeld</strong>
    <p>In relatie tot bovenstaand voorbeeld</p>
<pre data-enlighter-language="php">
/**/

// Een interface definieer je met het 'interface' keyword.
interface iDatabase {
    public function getAll($tableName);
    public function delete($id, $tableName);
}

class MysqlDatabase implements iDatabase {
    private $connection = null;

    public function __construct($host, $username, $password, $databaseName)
    {
    }

    public function getAll($tableName) {
        //$query = "SELECT * FROM {tableName}";
        //$result = mysqli_query($this->connection, $query);
        //return mysqli_fetch_all($result, MYSQLI_ASSOC);

        return array('Joop', 'Kees');
    }

    public function delete($id, $tableName) {
        return true;
    }
}

class PgsqlDatabase implements iDatabase {
    private $connection = null;

    public function __construct($host, $username, $password, $databaseName)
    {
    }

    public function getAll($tableName) {
        //$query = "SELECT * FROM {tableName}";
        //$result = pg_query($this->connection, $query);
        //return pg_fetch_all($result);
        return array('Piet', 'Klaas');
    }

    public function delete($id, $tableName) {
        return true;
    }
}

class User {

    const TABLE_NAME = 'users';
    private $database = null;

    // Hiermee geef je aan dat User een klasse kan verwachten die de iDatabase interface heeft geimplementeerd.
    public function __construct(iDatabase $database) {
        $this->database = $database;
    }

    public function fetch() {
        return $this->database->getAll(self::TABLE_NAME);
    }
}

$mysqlDatabase = new MysqlDatabase('localhost', 'root', 'root', 'blog');
$user = new User($mysqlDatabase);
print_r($user->fetch());

// Stel ik wil later wisselen van database engine
$postgresDatabase = new PgsqlDatabase('localhost', 'root', 'root', 'blog');
$user = new User($postgresDatabase);
print_r($user->fetch());

/**/
</pre>

<h2>Polymorphism</h2>
<p>Wanneer je een enkele interface definieerd voor verschillende typen klassen
    (Mysql vs Postgresl in bovenstaand voorbeeld) dan noem je dat polymorfisme.
</p>


<?php include '../../../templates/footer.php'; ?>