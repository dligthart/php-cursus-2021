<?php include '../templates/head.php'; ?>

<?php include '../templates/menu.php'; ?>

<div class="container py-5" id="custom-cards">
  <h2 class="pb-2 border-bottom">Lessen Overzicht</h2>

  <div class="row row-cols-3 align-items-stretch py-5">
      <!-- col 1 -->
    <div class="col">
      <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg cursor-pointer" style="background-image: url('');"
      onclick="window.location.href='/cursus/les1'">
        <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
          <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Les 1: Introductie</h2>
          <ul class="d-flex list-unstyled mt-auto">
            <li class="me-auto">
              <img src="/img/php.png" alt="php" width="32" height="32" class="rounded-circle border border-white">
            </li>
            <li class="d-flex align-items-center me-3">
              <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
              <small>Basics</small>
            </li>
            <li class="d-flex align-items-center">
              <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
              <small>19 Maart 2021</small>
            </li>
          </ul>
        </div>
      </div>
    </div>
      <!-- col 2 -->
    <div class="col">
      <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg cursor-pointer" style="background-image: url('');" onclick="window.location.href='/cursus/les2'">
        <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
          <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Les 2: PHP & MYSQL</h2>
          <ul class="d-flex list-unstyled mt-auto">
            <li class="me-auto">
              <img src="/img/php.png" alt="php" width="32" height="32" class="rounded-circle border border-white">
            </li>
            <li class="d-flex align-items-center me-3">
              <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
              <small>Database Handling</small>
            </li>
            <li class="d-flex align-items-center">
              <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
              <small>02 April 2021</small>
            </li>
          </ul>
        </div>
      </div>
    </div>
      <!-- col 3 -->
    <div class="col">
      <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg cursor-pointer" style="background-image: url('');"
      onclick="window.location.href='/cursus/les3'">
        <div class="d-flex flex-column h-100 p-5 pb-3 text-shadow-1">
          <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Les 3: OOP</h2>
          <ul class="d-flex list-unstyled mt-auto">
            <li class="me-auto">
              <img src="/img/php.png" alt="php" width="32" height="32" class="rounded-circle border border-white">
            </li>
            <li class="d-flex align-items-center me-3">
              <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
              <small>Object Oriented Programming</small>
            </li>
            <li class="d-flex align-items-center">
              <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
              <small>16 April 2021</small>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="row row-cols-3 align-items-stretch py-5">
      <!-- col 1 -->
    <div class="col">
      <div class="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg cursor-pointer" style="background-image: url('');"
      onclick="window.location.href='/cursus/les4'">
        <div class="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
          <h2 class="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Les 4: MVC</h2>
          <ul class="d-flex list-unstyled mt-auto">
            <li class="me-auto">
              <img src="/img/php.png" alt="php" width="32" height="32" class="rounded-circle border border-white">
            </li>
            <li class="d-flex align-items-center me-3">
              <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#geo-fill"/></svg>
              <small>MVC</small>
            </li>
            <li class="d-flex align-items-center">
              <svg class="bi me-2" width="1em" height="1em"><use xlink:href="#calendar3"/></svg>
              <small>30 April 2021</small>
            </li>
          </ul>
        </div>
      </div>
    </div>
      <!-- col 2 -->
    <div class="col">

    </div>
      <!-- col 3 -->
    <div class="col">
      
  </div>
</div>

<div class="container py-5" id="featured-3">
  <h2 class="pb-2 border-bottom">Downloads</h2>
  <div class="row g-5 py-5">
      <!-- col 1 -->
      <div class="feature col-md-4">
          <h2>Presentatie Les 1</h2>
          <p>Basics</p>
          <a href="/downloads/les1/presentatie-les1.pdf" class="icon-link" target="_blank" title="Presentatie Les 1">Download</a>
      </div>
      <!-- col 2 -->
      <div class="feature col-md-4">
          <h2>Presentatie Les 2</h2>
          <p>Database Handling</p>
          <a href="/downloads/les2/presentatie-les2.pdf" class="icon-link" target="_blank" title="Presentatie Les 2">Download</a>
      </div>
      <!-- col 3 -->
      <div class="feature col-md-4">
          <h2>Presentatie Les 3</h2>
          <p>Object-oriented Programming</p>
          <a href="/downloads/les3/presentatie-les3.pdf" class="icon-link" target="_blank" title="Presentatie Les 3">Download</a>
      </div>
  </div>
</div>

<?php include '../templates/footer.php'; ?>