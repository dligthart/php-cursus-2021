<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les1">Terug naar overzicht</a>

<h1 class="py-5 pb-2 border-bottom">Les 1.3 - Data types, If statements en Operators</h1>

<h2 class="py-5 pb-2">Data types</h2>

<pre data-enlighter-language="php">

// is een serie van characters mag tussen enkele of dubbele quotes.
$stringVariabele = "Hallo ik ben een string";

// heeft 1 getal en geen decimale punt tussen min -2,147,483,648 en max 2,147,483,647.
$intVariabele = 1;

// een getal met een decimale punt.
$floatVariabele = 1.01;

// true of false.
$booleanVariabele = true;

// geen toegewezen waarde.
$nullVariabele = null;

// je kan hier meerdere waarden opslaan in een variabele.
$arrayVariabele = array("Dit", "is", "een", "array");

</pre>

<!-- Hier toon ik een lijstje met informatie over de variabelen -->
<h3 class="py-5 pb-2"> Informatie over de variabelen </h3>
<pre data-enlighter-language="php">
$variabelen = array(
	"Hallo ik ben een string",
	1,
	1.01,
	true,
	null,
	array("Dit", "is", "een", "array")
);

// Toon informatie over deze variabele met de var_dump() methode.
var_dump($variabelen);

</pre>
<?php

// Deze functie verwijdert de html van de var_dump output.
function echo_var_dump($var) {
    ob_start();
    var_dump($var);
    $c = ob_get_contents();
    ob_end_clean();
    echo strip_tags($c);
}

$variabelen = array(
    "Hallo ik ben een string",
    1,
    1.01,
    true,
    null,
    array("Dit", "is", "een", "array")
);
?>
    <ul>
        <li>String: <?php echo_var_dump($variabelen[1]); ?></li>
        <li>Integer: <?php echo_var_dump($variabelen[1]); ?></li>
        <li>Float: <?php echo_var_dump($variabelen[2]); ?></li>
        <li>Boolean: <?php echo_var_dump($variabelen[3]); ?></li>
        <li>NULL: <?php echo_var_dump($variabelen[4]); ?></li>
        <li>Array: <?php echo_var_dump($variabelen[5]); ?></li>
    </ul>

<h2 class="py-5 pb-2">Code Comments</h2>

<pre data-enlighter-language="php">
/*
Met de functie 'var_dump' kun je informatie tonen  over de variabele.
En zoals je ziet kun je op deze wijze commentaar toevoegen over
meerdere regels van je code.
*/

// En zo kun je een single line comment maken.
</pre>

<h2 class="py-5 pb-2">If statements en operators</h2>

<h3 class="py-5 pb-2">Vergelijkingsoperatoren</h3>

<p>
    Een conditie vergelijkt twee waardes met elkaar en geeft <strong>true</strong> of <strong>false</strong> als waarde terug.
</p>

<pre data-enlighter-language="php">
$a = 1;
$b = 1;
if ($a == $b) { // gelijk aan.
&nbsp;&nbsp;echo 'a == b';
&nbsp;&nbsp;var_dump($a == $b);

}

$a = 2;
$b = 1;
if ($a > $b) { // groter dan maar niet gelijk.
&nbsp;&nbsp;echo 'a > b';
&nbsp;&nbsp;var_dump($a > $b);

}

$a = 1;
$b = 2;
if ($a < $b) { // kleiner dan maar niet gelijk.
&nbsp;&nbsp;echo 'a < b';
&nbsp;&nbsp;var_dump($a < $b);

}

$a = 2;
$b = 2;
if ($a >= $b) { // groter dan of gelijk.
&nbsp;&nbsp;echo 'a >= b';
&nbsp;&nbsp;var_dump($a >= $b);

}

$a = 2;
$b = 2;
if ($a <= $b) { // kleiner dan of gelijk.
&nbsp;&nbsp;echo 'a <= b';
&nbsp;&nbsp;var_dump($a <= $b);

}

$a = 1;
$b = 2;
if ($a <=> $b) {  // kleiner dan, gelijk, of groter dan.
&nbsp;&nbsp;echo 'a <=> b';
&nbsp;&nbsp;var_dump($a <=> $b);

}

if ($a != $b) { // niet gelijk aan.
&nbsp;&nbsp;echo 'a != b';
&nbsp;&nbsp;var_dump($a != $b);

}
</pre>

<h3 class="py-5 pb-2">Logische operatoren</h3>
<p>
Condities kunnen gecombineerd worden via logische operatoren.
<br/>
Er zijn er drie:
</p>
<ul>
	<li>AND : <strong>&&</strong> of <strong>and</strong> </li>
	<li>OR: <strong>||</strong> of <strong>or</strong></li>
	<li>NOT: <strong>!</strong> </li>
	<li>XOR: <strong>xor</strong> </li>
</ul>

AND: waarbij conditie1 en conditie2 beide true zijn.
<pre>
	conditie1 && conditie2
</pre>
<br/>
OR: waarbij of conditie1 of conditie2 true is.
<pre>
	conditie1 || conditie2
</pre>
<br/>
NOT: waarbij true of false wordt omgekeerd.
<pre>
	!conditie1
</pre>
<br/>
XOR: waarbij conditie1 of conditie2 true is maar niet allebei.
<pre>
	conditie1 xor conditie2
</pre>

<h3 class="py-5 pb-2">String operatoren</h3>
<p>
Strings aan elkaar plakken door middel van de punt operator: .
<pre data-enlighter-language="php">
	$hello_world = $hello . ' ' . $world;

	// of

	$hello_world .= $hello;

	$hello_world .= ' ';

	$hello_world .= $world;
</pre>
</p>

<h3 class="py-5 pb-2">Voorbeeld:</h3>
<pre data-enlighter-language="php">
$temperatuur = rand(-10, 40);

echo 'De temperatuur is: ' . $temperatuur . ' graden Celcius.';

echo '<br/>';

if ($temperatuur >= 10 && $temperatuur <= 20) {
    echo 'Milde temperatuur';
} elseif ($temperatuur < 10 ) {
    echo 'Koude temperatuur';
} else {
	echo 'Warme temperatuur';
}

echo '<br/>';

if (!$temperatuur) {
	echo 'Het is precies 0 graden';
} elseif($temperatuur < 0) {
	echo 'Het is kouder dan 0 graden';
} elseif($temperatuur > 0) {
	echo 'Het is warmer dan 0 graden';
}

echo '<br/>';

if($temperatuur < 0 || $temperatuur > 30) {
	echo 'De temperatuur is extreem';
}

</pre>

<?php include '../../../templates/footer.php'; ?>