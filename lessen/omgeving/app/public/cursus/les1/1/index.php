<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

    <a href="/cursus/les1">Terug naar overzicht</a>

    <h1 class="py-5 pb-2 border-bottom">Les 1.1 - HTML en Echo statement</h1>

    <h2 class="py-5 pb-2">HTML</h2>

    <pre data-enlighter-language="html">
    &lt;!DOCTYPE html&gt;
    &lt;html&gt;
    &lt;body&gt;
    &lt;h1&gt;Hallo Wereld&lt;/h1&gt;
    &lt;/body&gt;
    &lt;/html&gt;
    </pre>

    <h2 class="py-5 pb-2">Tonen van <?php echo "Hallo Wereld"; ?></h2>
    <p>

    Je mag echo zo aanroepen:

    <pre data-enlighter-language="php">
        &lt;php
        echo "Hallo Wereld";
        ?&gt;
    </pre>

    Statements eindigen altijd met een <strong>;</strong>

    </p>

<?php include '../../../templates/footer.php'; ?>