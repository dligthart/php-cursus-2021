<!DOCTYPE html>
<html>
<body>

<a href="/cursus/les1">Terug naar overzicht</a>

<h1>Les 1.1 - Oefeningen</h1>

<h2>Oefening 1 - Installeren omgeving</h2>

<ol>
	<li><a href="https://gitlab.com/dligthart/php-cursus-2021/-/tree/master" target="_blank" title="Cursus PHP 2021">Download de cursus</a>. Dit kan middels de <a href="https://desktop.github.com/" target="_blank">github desktop client</a> (geschikt voor Windows en MacOS).</li>
	<li>Installeer en start een ontwikkelomgeving op jouw computer volgens de instructies in het README.md bestand. </li>
	<li>De uitwerkingen van de oefeningen kun je plaatsen in de map "uitwerkingen". Je kan hier een "index.php" bestand in plaatsen.</li>
</ol>


<h2>Oefening 2 - HTML</h2>

<p><em>* Je kunt de uitwerkingen plaatsen in een index.php bestand in de map "uitwerkingen".</em></p>

<p> 
	Maak een valide HTML document aan en plaats deze in het index.php bestand in de map uitwerkingen.
</p>

<h2>Oefening 3 - PHP Statement</h2>

<p>
	Toon de tekst "Hello World" op deze pagina door het echo statement.
</p>

</body>
</html> 