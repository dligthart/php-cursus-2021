<!DOCTYPE html>
<html>
<body>

<a href="/cursus/les1">Terug naar overzicht</a>

<h1>Les 1.2 - Oefeningen</h1>

<p><em>* Je kunt de uitwerkingen plaatsen in een index.php bestand in de map "uitwerkingen".</em></p>

<h2>Oefening 1</h2>
<p>
	Zet de tekst 'Hallo Wereld' in een variabele en toon deze op het scherm met het "echo" statement.
</p>

<h2>Oefening 2</h2>

<ol>
	<li>Wat is hier fout? <blockquote>$_My_var = "hallo"</blockquote></li>
	<li>Wat is hier fout? <blockquote>$my_var0 = "hallo";</blockquote></li>
	<li>Wat is hier fout? <blockquote>$!000_ = "hallo";</blockquote></li>
	<li>Wat is hier fout? <blockquote>$1_hallo_ = "hallo";</blockquote></li>
</ol>

<h2>Oefening 3</h2>
<p>
De volgende gegevens moeten in aparte variabelen met een goede naamgeving bewaard worden:
</p>
<ul>
	<li>Aanhef</li>
	<li>Voornaam</li>
	<li>Achternaam</li>
	<li>Straat</li>
	<li>Postcode</li>
	<li>Woonplaats</li>
	<li>Land</li>
</ul>

Vul de variabelen met fictieve gegevens. En toon de waarde op het scherm.

</body>
</html> 