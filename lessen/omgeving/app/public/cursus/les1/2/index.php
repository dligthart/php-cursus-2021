<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les1">Terug naar overzicht</a>	

<h1 class="py-5 pb-2 border-bottom">Les 1.2 - Variabelen</h1>

<pre data-enlighter-language="php">
	&lt;?php
	$title = "Hallo Wereld";
	echo $title;
	?&gt;
</pre>

Regels:
<ul>
	<li>Een variable declareer je met een '$' teken</li>
	<li>Mag beginnen met een _, hoofd- of kleine letter</li>
	<li>Alleen de a t/m z, 0 t/m 9 en _ karakters zijn toegestaan</li>
</ul>

<?php include '../../../templates/footer.php'; ?>