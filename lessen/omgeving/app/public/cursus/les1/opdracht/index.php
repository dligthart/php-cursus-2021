<!DOCTYPE html>
<html>
<body>

<a href="/cursus/les1">Terug naar overzicht</a>

<h1>Les 1 - Opdracht</h1>

<h2></h2>

<p>

Schrijf een programma met de volgende functionaliteiten:

	<ol>
		<li>Grove woorden uit willekeurige teksten vervangen voor willekeurige (random) aardige woorden.</li>
		<li>Zinnen automatische corrigeert op hoofdletters.</li>
		<li>Tonen van de de gecorrigeerde tekst.</li>
		<li>De tekst die getoond wordt mag niet langer zijn dan (x) aantal tekens en worden afgebroken met "..."</li>
		<li>Tonen van de grofheid van de tekst; met indicator "rood", "geel" of "groen".
		<li>Tonen van statistieken: 1. Aantal woorden, 2. Aantal vervangen woorden, 3. Aantal gecorrigeerde hoofdletters. 4. Percentage vervangen woorden. Bij voorkeur in een tabel.</li>
		<li>Zorg voor herbruikbaarheid van de code door functies te maken die een enkele taakuitvoeren.</li>
		<li>Test de output van jouw programma op de correcte werking door de functies verschillende teksten en parameters mee te geven.</li>
	</ol>

</p>

* Vrijblijvende bonus opdracht: verzin zelf een aantal leuke uitbreidingen op het programma.

</body>
</html> 