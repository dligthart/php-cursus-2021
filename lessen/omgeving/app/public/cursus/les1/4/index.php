<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les1">Terug naar overzicht</a>

<h1 class="py-5 pb-2 border-bottom">Les 1.4 - Arrays, Functies, String functies</h1>

<h2 class="py-5 pb-2"> Arrays </h2>

<h3 class="py-5 pb-2">Array</h3>

<pre data-enlighter-language="php">
	
	$autos = array("Volvo", "BMW", "Toyota");

	echo "Ik hou van " . $autos[0] . ", " . $autos[1] . " en " . $autos[2];

</pre>

<h4 class="py-5 pb-2">Nieuwe waarde aan een array toevoegen</h4>

<pre data-enlighter-language="php">
	$autos[] = "Mercedes";
</pre>

<h4 class="py-5 pb-2">Een waarde verwijderen uit een array</h4>

<pre data-enlighter-language="php">
	$minderAutos = array_diff($autos, array("BMW"));
</pre>

<h3 class="py-5 pb-2">Associatieve array</h3>

<pre data-enlighter-language="php">
	
	$leeftijden = array("Peter" => "31", "Ben" => "32");

	echo "Peter is " . $leeftijden['Peter'] . " jaar oud.";

</pre>

<h3 class="py-5 pb-2">Multi-dimensionale array</h3>

<pre data-enlighter-language="php">
	
$autos = array (
&nbsp;&nbsp;array("Volvo",22,18),
&nbsp;&nbsp;array("BMW",15,13),
&nbsp;&nbsp;array("Saab",5,2),
&nbsp;&nbsp;array("Land Rover",17,15)
);

echo "Merk: " . $autos[0][0]; 

echo "In voorraad: " . $autos[0][1];

echo "Verkocht: " . $autos[0][2];

echo "Merk: " . $autos[1][0];

echo "In voorraad: " . $autos[1][1];

echo "Verkocht: " . $autos[1][2];

// etc..

</pre>

<h2 class="py-5 pb-2">Functies</h2>
<p>
Declareer een functie:
</p>
<pre data-enlighter-language="php">

function sayHelloWorld() {
&nbsp;&nbsp;return "Hello World";
}

echo sayHelloWorld();
	
</pre>

<p>
Met functie argumenten:
</p>

<pre data-enlighter-language="php">
function sayHelloWorld($first_name, $last_name) {
&nbsp;&nbsp;return "Hello World, " . $first_name . "," . $last_name . !";
}

echo sayHelloWorld("Kees", "de Boer");
</pre>

<h2 class="py-5 pb-2">String functies</h2>

<pre data-enlighter-language="php">
$title = "Hallo Wereld";

$tekst = "Cultivar a, trifecta instant skinny, espresso, con panna, crema spoon mocha, in coffee, sugar, french press medium latte trifecta instant to go. Breve skinny cinnamon grounds grinder, cortado, dark cup, crema percolator turkish, decaffeinated aromatic aftertaste redeye iced chicory. Single origin, steamed at seasonal, aged iced galão aftertaste beans sweet mug, extra  filter, in, cappuccino, white con panna, frappuccino aftertaste frappuccino qui chicory. Grinder medium et so, and java, trifecta, at, extra  café au lait trifecta, organic blue mountain coffee dark single origin. Viennese, aroma filter saucer cinnamon and, white, to go, crema coffee so lungo grounds, cultivar percolator french press acerbic americano siphon. Steamed eu est blue mountain, mug decaffeinated cortado strong, so as mug espresso acerbic sugar french press aroma."

// De lengte van de titel is:
echo strlen($title);

// De string omkeren:
echo strrev($title);

// Vervangen van de woorden in een string:
echo str_replace('Hallo', 'Gegroet', $title);

// Het aantal woorden in de tekst is:
echo str_word_count($tekst);

// Het tonen van de waarde van een variabele in een string:
$wereld = "Allemaal";
echo "Hallo $wereld";

</pre>

<p>
Zie ook <a href="https://www.php.net/manual/en/ref.strings.php" target="_blank">php.net string functies</a>
<br/>

<?php include '../../../templates/footer.php'; ?>