<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les1">Terug naar overzicht</a>
	
<h1 class="py-5 pb-2 border-bottom">Les 1.5 - Loops</h1>

<h2 class="py-5 pb-2">De "For loop"</h2>
<pre data-enlighter-language="php">
$words = array("hallo", "wereld,", "goedemorgen");

$sentence = '';

for ($index = 0; $index < count($words); $index++) {

	$sentence .= $words[$index];

	if ($index == count($words) - 1) {
		
		$sentence .= '!';
	} 
	else {
		
		$sentence .= ' ';
	}
}

echo $sentence;
</pre>

<h2 class="py-5 pb-2">De "Foreach loop"</h2>

<pre data-enlighter-language="php">
foreach($words as $word) {
	echo $word . ' ';
}

</pre>

<h2 class="py-5 pb-2">De "While loop"</h2>
<pre data-enlighter-language="php">

$i = 0;
while(count($words) != $i) {
	echo strtoupper($words[$i]);
	echo ' ';
	$i++;
}

</pre>

<?php include '../../../templates/footer.php'; ?>