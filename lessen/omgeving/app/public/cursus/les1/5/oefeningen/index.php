<!DOCTYPE html>
<html>
<body>

<a href="/cursus/les1">Terug naar overzicht</a>	

<h1>Les 1.5 - Oefeningen</h1>

<p><em>* Je kunt de uitwerkingen plaatsen in een index.php bestand in de map "uitwerkingen".</em></p>

<h2>Oefening 1</h2>

<p>
Schrijf een programma waarmee onderstaande data in een tabel wordt getoond met headers en de waarde in ieder een eigen kolom.
<br/>
<em>*hint: het gaat hier om keys en values.</em>
</p>

<blockquote>
$students = <br/>
&nbsp;&nbsp;array( <br/>
&nbsp;&nbsp;&nbsp;&nbsp;array("first_name" => "Piet", "score" => 8.3, "last_name" => "Smith"), <br/>
&nbsp;&nbsp;&nbsp;&nbsp;array("first_name" => "Jan", "score" => 9.2, "last_name" => "de Boer"), <br/>
&nbsp;&nbsp;&nbsp;&nbsp;array("first_name" => "Anna", "score" => 9.0, "last_name" => "Bel")  <br/>
);	
</blockquote>

<h2>Oefening 2</h2>

<p>Schrijf code om een extra kolom toe te voegen genaamd "student_id" en geef iedere leerling een eigen nummer.</p>

<h2>Oefening 3</h2>
<p>Toon de rijen in de tabel van laagste cijfer naar hoogste cijfer.</p>

<h2>Oefening 4</h2>
<p>Geef de rij met de leerling met het hoogste cijfer een groene achtergrond kleur.</p>

</body>
</html>