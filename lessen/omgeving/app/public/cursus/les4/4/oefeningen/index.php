<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les4">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 4.4 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

    <ul>
        <li>Maak een Config klasse waarmee je configs kunt getten en setten en verwijderen: </li>
        <li>Voorbeeld: Config::get('db.connection.host'); Config::set('db.connection.host', '127.0.0.1'); Config.delete('db.connection.host');</li>
        <li>Hint: gebruik een array in de config klasse als statische property</li>
    </ul>

</div>

<?php include '../../../../templates/footer.php'; ?>