<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

	<a href="/cursus/les4">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 4.4</h1>

    <h2 class="py-5 pb-2">Static sleutelwoord en binding</h2>

    <p>
        Door klasse eigenschappen of methoden als statisch te declareren, worden ze toegankelijk zonder dat de klasse hoeft te worden geïnstantieerd. Hiermee kun je ze overal (globaal) gebruiken binnen je programma. Deze zijn ook statisch toegankelijk binnen een geïnstantieerd object.
    </p>


<strong>Voorbeeld</strong>

<pre data-enlighter-language="php">

class Test
{
   public static $count = 0;
   
   public function __construct(){
      self::$count++;
   }
   
   public static function showCount(){
      echo "count = " . self::$count;
   }
}

$a=new Test(); 
$b=new Test();
$c=new Test();

Test::showCount(); // toont 3

</pre>

<strong>Voorbeeld</strong>
<pre data-enlighter-language="php">
/**/

class Post 
{
	
	// Statische property aanmaken dmv "static" sleutelwoord:
	public static $title = 'Hello world';

	// Statische functie aanmaken dmv "static" sleutelwoord:
    public static function getTitle() {
        return self::$title;  // self refereert naar de scope van de huidige instantie.
    }
}

echo Post::getTitle();

/**/
</pre>

<h2 class="py-5 pb-2">Late binding</h2>


<strong>Voorbeeld</strong>
<pre data-enlighter-language="php">
/**/

class Post 
{

	public static $title = 'This is a default post';

	public static function setTitle($title) {
		self::$title = $title;
	}

    public static function getTitle() {
        return static::$title; // static refereert aan de huidige klasse instantie tijdens runtime.	
    }
}

class BlogPost extends Post 
{
	
	public static $title = 'This is a blog post'; // hier overschrijven we de titel van de superklasse.

}

echo Post::getTitle(); // toont "this is a default post"

echo '<br/>';

echo BlogPost::getTitle(); // toont "this is a blog post"

BlogPost::setTitle('Test');

echo BlogPost::getTitle(); // toont "Test"

/**/
</pre>


<?php include '../../../templates/footer.php'; ?>