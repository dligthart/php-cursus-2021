<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

	<a href="/cursus/les4">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 4.3</h1>

    <h2 class="py-5 pb-2">Abstracte klasse</h2>

    <p>
    	Een abstracte klasse is een klasse die functionaliteit implementeert en kan delen met de subklassen en daarnaast bepaald welke methodes een subklasse moet implementeren.
    </p>

<strong>Voorbeeld</strong>

<pre data-enlighter-language="php">
/* php */

// De klasse krijgt het keyword "abstract"
abstract class User 
{
    private $_username;

    public function __construct($name) {
        $this->_username = $name;
    }

    // De implementatie van een gedeelde methode.
    public function getUsername() {
        return $this->_username;
    }
    
    // De methode krijgt het keyword 'abstract'
    // Hiermee dwing je af dat de superklasse deze MOET implementeren. 
    // Net zoals bij interfaces.
    public abstract function getUserRole();
}

class Admin extends User 
{

    public function __construct($username) {
        parent::__construct($username);
    }
    
    // Deze methode is bepaald door de abstracte superklasse.
    public function getUserRole() {
        return 'administrator';
    }
}

$admin = new Admin('Karel');

// De gedeelde methode van de superklasse:
echo $admin->getUsername();
echo ' is een ';

// De geimplementeerde abstracte methode door de subklasse.
echo $admin->getUserRole();

// Dit mag NIET:
$user = new User('Pietje'); 
// van een abstracte klasse kunnen er geen instanties worden aangemaakt!


/**/
</pre>


<?php include '../../../templates/footer.php'; ?>