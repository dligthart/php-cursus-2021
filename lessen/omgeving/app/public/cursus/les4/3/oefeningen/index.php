<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les4">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 4.3 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

    <ul>
        <li>Maak een abstracte klasse van model en de volgende abstracte methodes: store(), load(), trash(). De store methode slaat het object op in de database, de load methode laad het object uit de database, de trash methode verwijdert het object uit de database.</li>

        <li>Maak de volgende gedeelde methodes: getId(), getTableName(), getId geeft het id terug, getTableName geeft de naam van de database tabel terug.</li>

        <li>Maak de volgende implementaties van model: User, Post en Comment</li>
     
    </ul>

</div>

<?php include '../../../../templates/footer.php'; ?>