<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les4">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 4.2 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

    <ul>
        <li>Maak de router van het uitgebreide voorbeeld (zie theorie) werkend voor blog posts.</li>
        <li>Implementeer de MVC voor blog posts in de router.</li>
    </ul>

</div>

<?php include '../../../../templates/footer.php'; ?>