<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

	<a href="/cursus/les4">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 4.2</h1>

    <h2 class="py-5 pb-2">Routing</h2>

    <p>
         
    </p>


<h2>Gesimplificeerd voorbeeld</h2>
<pre data-enlighter-language="php">
/* php */


// Wanneer er url variabelen worden meegegeven worden deze opgeknipt in een array. voorbeeld: http://localhost/blog/users.php?id=1

$request_uri = explode('?', $_SERVER['REQUEST_URI'], 2);

/* 

Start met routeren dmv een switch statement, deze werkt als een 

if($request_uri[0 == '/blog'){
	
}
elseif($request_uri[0] == '/blog/users'){
	
}
elseif{
	// 404.
} 

etc..

*/

switch ($request_uri[0]) {
    case '/blog':
        require 'home.php';
        break;
    case '/blog/users':
        require 'users.php';
        break;
    default:
    	// Wanneer er een pagina niet gevonden kan worden toon je een
    	// standaard 404 pagina.
        header('HTTP/1.0 404 Not Found');
        require '404.php';
        break;
}

exit;

/**/
</pre>

<h2>Uitgebreider voorbeeld</h2>
<pre data-enlighter-language="php">
/* php */

// De huidige route en query variabelen.
$request_uri = explode('?', $_SERVER['REQUEST_URI'], 2);

// De action method die wordt aangeroepen van de controller.
$action = '';
if (isset($_GET['action'])) {
	$action = $_GET['action'];
}

// Het id van het model, je kan hier bijvoorbeeld update en delete actions mee doen.
$id = -1;
if (isset($_GET['id'])) {
	$id = $_GET['id'];
} 

switch ($request_uri[0]) {
    case '/blog':
        require 'home.php';
        break;
    case '/blog/users':
        
    	$model = new User($id);

			$controller = new UserController($model);

			// Wanneer !$id (not id) kun je een getAll() op het model 
			// in de view om alle users te tonen.
			$view = new UserView($controller, $model);

			if(!empty($action)) {
				$controller->{$action . 'Action'}();
			}

			echo $view->fetch();

        break;
    default:
        header('HTTP/1.0 404 Not Found');
        require '404.php';
        break;
}

/**/
</pre>
<?php include '../../../templates/footer.php'; ?>