<?php include '../../templates/head.php'; ?>

<?php include '../../templates/menu.php'; ?>

<div class="container">

  <h2 class="pb-2 border-bottom">Les 4 - 30 April 2021</h2>

	<h3 class="py-2 pb-2">Theorie</h3>
	<ul>
		<li>
			<a href="/cursus/les4/1" title="Theorie 1">Theorie 1</a>
		</li>
		<li>
			<a href="/cursus/les4/2" title="Theorie 2">Theorie 2</a>
		</li>
		<li>
			<a href="/cursus/les4/3" title="Theorie 3">Theorie 3</a>
		</li>
		<li>
			<a href="/cursus/les4/4" title="Theorie 4">Theorie 4</a>
		</li>
		<!-- <li>
			<a href="/cursus/les4/5" title="Theorie 5">Theorie 5</a>
		</li> -->
	</ul>

	<h3 class="py-2 pb-2">Oefeningen</h3>
	<ul>
		<li>
			<a href="/cursus/les4/1/oefeningen" title="Oefening 1">Oefening 1</a>
		</li>
		<li>
			<a href="/cursus/les4/2/oefeningen" title="Oefening 2">Oefening 2</a>
		</li>
		<li>
			<a href="/cursus/les4/3/oefeningen" title="Oefening 3">Oefening 3</a>
		</li>
		<li>
			<a href="/cursus/les4/4/oefeningen" title="Oefening 4">Oefening 4</a>
		</li>
		<!-- <li>
			<a href="/cursus/les4/5/oefeningen" title="Oefening 5">Oefening 5</a>
		</li> -->
	</ul>

	<h3 class="py-2 pb-2">Opdracht</h3>

	<a href="/cursus/les4/opdracht">Opdracht</a>

	<p>&nbsp;</p>

</div>

<?php include '../../templates/footer.php'; ?>