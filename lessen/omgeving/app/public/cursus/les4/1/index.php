<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

	<a href="/cursus/les4">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 4.1</h1>

    <h2 class="py-5 pb-2">Model-View-Control Pattern (MVC)</h2>

    <p>
      Het model-view-control (MVC) pattern is in de jaren 70 van de vorige eeuw bedacht om de presentatie logica te scheiden van de logica die bewerkingen doet op de data. Het wordt veel gebruikt in web applicaties om herbruikbaarheid van code te bevorderen. Zo kun je een web applicatie makkelijker uitbreiden of wijzigen met nieuwe functionaliteiten.   
    </p>

    <p>MVC bestaat uit drie onderdelen: 

    <ul>
        <li>Model: zorgt voor de correcte opslag van data en is de brug tussen de View en de Controller</li>
        <li>View: Hier wordt de html gegenereerd en getoond aan de gebruiker, de view maakt gebruik van de data uit het model.</li>
        <li>Controller: Het doel acties af te handelen vanuit de view en data te verzamelen om door te sturen aan het model voor opslag. De gebruiker drukt op een knop in de view, de controller handeld deze actie af. De data op een correcte wijze opslaan of uitlezen mag alleen door het model gedaan worden. Deze logica mag niet in de controller worden neergezet.</li>
    </ul>   

    <div class="text-center">
        <img src="/img/mvc.png" class="img-responsive" />
    </div>

    

<h2>Gesimplificeerd voorbeeld</h2>
<pre data-enlighter-language="php">
/* php */

class Model 
{
    private $string;

    public function __construct($string) {
        $this->string = $string;
    }

    public function getString() {
        return $this->string;
    }

    public function setString($string) {
        $this->string = $string;
    }

}

class View 
{ 

    private $model;
    private $controller;
    private $html;

    public function __construct(Controller $controller, Model $model) {
        $this->controller = $controller;
        $this->model = $model;
    }

    private function render() {
        $this->html  = '<div>';
        $this->html .= '<h1>' . $this->model->getString() . '</h1>';
        $this->html .= '<a href="?action=click">Klik actie uitvoeren</a>';
        $this->html .= '</div>';
    }

    public function fetch() {
        $this->render();
        return $this->html;
    }
}

class Controller 
{
    private $model;

    public function __construct(Model $model) {
        $this->model = $model;
    }

    public function clickAction() {
        $this->model->setString('Er is geklikt!');
    }
}


$model = new Model('Hallo Wereld');

$controller = new Controller($model);

$view = new View($controller, $model);

if (isset($_GET['action']) && !empty($_GET['action'])) {
    // Hier wordt de methode in de controller aangeroepen op basis van de waarde van de action parameter.
    $controller->{$_GET['action'] . 'Action'}();
}

echo $view->fetch();

/**/
</pre>


<h2>Uitgebreider voorbeeld met overerving</h2>

<pre data-enlighter-language="php">
/* php */

class Model 
{
    
    private $id; 

    public function __construct($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function delete() {
        if($this->id) { 
            // delete from database
            $this->id = -1;
        }
    }
}


class User extends Model 
{
    
    private $username;

    public function __construct($id, $username) {
        parent::__construct($id);

        $this->username = $username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getUsername() {
        return $this->username;
    }

    // Hier overschrijven we de methode delete van de superklasse.
    // we maken ene eigen implementatie hiervan en roepen hier expliciet
    // de methode van de superklasse aan.
    public function delete() {

        parent::delete();

        $this->username = '';
    }

}

// Alle methodes in een abstracte klasse moeten door de subklasse worden geimplementeerd.
// Dit is een alternatief voor interfaces omdat je hierbij methodes kunt implementeren 
// die geërfd kunnen worden door subklasses.
abstract class View 
{
    
    protected $html;

    // moet door subklasse worden geimplementeerd.
    abstract protected function render();

    // gedeelde methode
    public function fetch() {
        $this->render();
        return $this->html;
    }
}

class UserView extends View 
{ 

    private $userModel;
    private $controller;

    public function __construct(Controller $controller, User $model) {
        $this->controller = $controller;
        $this->userModel = $model;
    }

    protected function render() {

        $this->html = '<div>';

        // Toon iets aan de gebruiker bij een delete actie.
        if (isset($_GET['action']) && $_GET['action'] == 'delete') {
            $this->html .= "<h1> User {$_GET['id']} deleted !</h1>";
        }

        // Toon alleen records van bestaande gebruikers.
        if ($this->userModel->getId() > -1) { 
            
            // Deze strings zijn leeg om dat de code preview iets geks doet. Ik heb geen idee waarom.
            // Je kan deze code in de folder les4/1/index.php bekijken.

            $this->html .= '<table class="table">';
            $this->html .= '<tr>';
            $this->html .= '<td>Id</td>';
            $this->html .= '<td>Username</td>';
            $this->html .= '<td>Actions</td>';
            $this->html .= '</tr>';

            // In dit voorbeeld wordt er maar 1 record getoond. 
            // Je kunt hier uiteraard meerdere records tonen uit de database. 
            // Dit vereist dat je het model uitbreid met functionaliteit 
            // om meerdere records te laden uit de database via het model. 
            // Maak bijvoorbeeld een methode in het model `$this->userModel->getAll();`  
            // (die vervolgens een 'SELECT * FROM users' doet ).
            // Middels een for loop de users records tonen in de view.

            $this->html .= '<tr>';
            $this->html .= '<td>' . $this->userModel->getId() . '</td>';
            $this->html .= '<td>' . $this->userModel->getUsername() . '</td>';
            $this->html .= '<td><a href="?action=delete&id=' . $this->userModel->getId() . '">Delete</a></td>'; 
            $this->html .= '</tr>';

            $this->html .= '</table>';      
        }

        $this->html .= '</div>';
    }
}

interface Controller 
{

    public function deleteAction();
}

class UserController implements Controller 
{
    private $model;

    public function __construct(Model $model) {
        $this->model = $model;
    }

    public function deleteAction() {
        $this->model->delete();
    }
}


$model = new User(1, 'kees');

$controller = new UserController($model);

$view = new UserView($controller, $model);

if (isset($_GET['action']) && !empty($_GET['action'])) {
    $controller->{$_GET['action'] . 'Action'}();
}

echo $view->fetch();

/**/
</pre>

<?php include '../../../templates/footer.php'; ?>