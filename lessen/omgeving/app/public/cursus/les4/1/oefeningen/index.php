<?php include '../../../../templates/head.php'; ?>

<?php include '../../../../templates/menu.php'; ?>

<div class="container">

	<a href="/cursus/les4">Terug naar overzicht</a>

	<h1 class="py-5 pb-2 border-bottom">Les 4.1 - Oefeningen</h1>

	<h2 class="py-5 pb-2">Oefening 1</h2>

    <p>Let op : zie uitgebreide voorbeeld theorie.</p>

    <ul>
        <li>Maak een model, view en controller aan voor blog posts</li>
        <li>Via het model kun je een blog post aanmaken (store) en verwijderen (trash)
            maak deze functioneel middels methodes mbv insert en delete sql queries in het model.
        </li>
        <li>Via de view toon je alle blog posts</li>
    </ul>

</div>

<?php include '../../../../templates/footer.php'; ?>