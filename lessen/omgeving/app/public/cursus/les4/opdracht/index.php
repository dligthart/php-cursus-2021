<?php include '../../../templates/head.php'; ?>

<?php include '../../../templates/menu.php'; ?>

<a href="/cursus/les3">Terug naar overzicht</a>

<h1 class="py-5 pb-2 border-bottom">Les 4 - Opdracht</h1>

<h2 class="py-5 pb-2">Het blog systeem uitbreiden met de volgende functionaliteiten:</h2>

<ol>
        <li>Implementeer de MVC pattern in jouw blog systeem: Denk aan UsersController, PostsController, CommentsController, AuthenticationController etc</li>
        <li>Maak voor Posts, Users, Comments, Login etc verschillende Views aan en laad de data uit het model</li>
        <li>Pas de views aan zodat je kunt toevoegen, wijzigen en verwijderen van Users, Posts, Comments</li>
        <li>Verbeter je model door er een abstracte klasse van te maken en implementeer methodes denk aan (load, store, trash, refresh, loadAll).
        <li>Maak een router klasse voor het blog systeem en implementeer deze zodat alle requests centraal naar de juiste controllers worden gerouteerd.</li>
        <li>Verander waar gedeelde methodes nodig zijn de interfaces voor abstracte klassen</li>
</ol>

<?php include '../../../templates/footer.php'; ?>