## Doel: Maak een blogsysteem in PHP 

### Functionele eisen

- Gebruikers beheren, rollen: admin, author en subscriber
- Posts beheren
- Comments beheren
- Media beheren
- Lezen van samenvattingen en volledige content
- Een homepage met overzicht van de meest recente content
- Inloggen, Uitloggen, Registreren